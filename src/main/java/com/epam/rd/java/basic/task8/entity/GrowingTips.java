package com.epam.rd.java.basic.task8.entity;

import java.math.BigInteger;

public class GrowingTips {

    private BigInteger temperature;
    private BigInteger watering;

    // attributes
    private static final String temperatureMeasure = "celsius";
    private static final String wateringMeasure = "mlPerWeek";
    private LightRequiring lightRequiring;

    public GrowingTips() {}

    public GrowingTips(BigInteger temperature, BigInteger watering, LightRequiring lightRequiring,
                       String tempMeasure, String waterMeasure) {
        this.temperature = temperature;
        this.watering = watering;
        this.lightRequiring = lightRequiring;

        validation(temperatureMeasure, tempMeasure);
        validation(wateringMeasure, waterMeasure);
    }

    public BigInteger getTemperature() {
        return temperature;
    }

    public void setTemperature(BigInteger temperature) {
        this.temperature = temperature;
    }

    public BigInteger getWatering() {
        return watering;
    }

    public void setWatering(BigInteger watering) {
        this.watering = watering;
    }

    public LightRequiring getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = LightRequiring.getRequiring(lightRequiring);
    }

    public static String getTemperatureMeasure() {
        return temperatureMeasure;
    }

    public static String getWateringMeasure() {
        return wateringMeasure;
    }

    @Override
    public String toString() {
        return "GrowingTips[" +
                "temperature=" + '\'' + temperature +
                ' ' + temperatureMeasure + '\'' +
                ", watering=" + '\'' + watering + ' ' +
                wateringMeasure + '\'' + ", " +
                lightRequiring +
                ']';
    }

    private void validation(String actualMeasure, String providedMeasure) {
        if(!actualMeasure.equals(providedMeasure)) {
            throw new IllegalArgumentException("Provided wrong measure" + '\n' +
                    "must be " + '\'' + actualMeasure + '\'');
        }
    }

}
