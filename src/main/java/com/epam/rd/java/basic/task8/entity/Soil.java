package com.epam.rd.java.basic.task8.entity;

public enum Soil {
    PODZOL("подзолистая"),
    GROUND("грунтовая"),
    SOD_PODZOLIC("дерново-подзолистая");

    private final String typeSoil;

    Soil(String typeSoil) {
        this.typeSoil = typeSoil;
    }

    public static Soil getSoil(String type) {
        for(Soil el: Soil.values()) {
            if(type.equals(el.value())) {
                return el;
            }
        }
        throw new IllegalArgumentException("Provided wrong type of soil");
    }

    public String value() {
        return typeSoil;
    }

    @Override
    public String toString() {
        return "Soil[" +
                "typeSoil='" + value() + '\'' +
                ']';
    }
}
