package com.epam.rd.java.basic.task8.constants;

public class Link {

    public static final String FEATURE_TURN_VALIDATION_ON = "http://xml.org/sax/features/validation";
    public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON = "http://apache.org/xml/features/validation/schema";
    public static final String ROOT_ATTRIBUTE_NAME_1 = "xmlns";
    public static final String ROOT_ATTRIBUTE_VALUE_1 = "http://www.nure.ua";
    public static final String ROOT_ATTRIBUTE_NAME_2 = "xmlns:xsi";
    public static final String ROOT_ATTRIBUTE_VALUE_2 = "http://www.w3.org/2001/XMLSchema-instance";
    public static final String ROOT_ATTRIBUTE_NAME_3 = "xsi:schemaLocation";
    public static final String ROOT_ATTRIBUTE_VALUE_3 = "http://www.nure.ua input.xsd ";
    public static final String STRING_EMPTY = "";


    private Link() {}
}
