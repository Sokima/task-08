package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Link;
import com.epam.rd.java.basic.task8.controller.helpers.Reader;
import com.epam.rd.java.basic.task8.entity.Flower;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.util.Collections;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends Reader {

	private final String xmlFileName;
	private List<Flower> flowers;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public static void main(String[] args) throws XMLStreamException {
		STAXController staxController = new STAXController("input.xml");
		staxController.parse();

		List<Flower> flowers = staxController.getFlowers();
		System.out.println(flowers);
	}

	// PLACE YOUR CODE HERE

	public void parse() throws XMLStreamException {

		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			if(event.isStartElement()) {
				startProcessing(getTagName(event), getAttributeValue(event));
			} else if(event.isCharacters()) {
				contentProcessing(getContent(event));
			} else if(event.isEndElement()) {
				endProcessing();
			} else if(event.isEndDocument()) {
				flowers = getCurrentList();
			}
		}

	}

	private String getTagName(XMLEvent event) {
		return event.asStartElement().getName().getLocalPart();
	}

	private String getAttributeValue(XMLEvent event) {
		return event.asStartElement().getAttributes().hasNext() ?
				event.asStartElement().getAttributes().next().getValue() : Link.STRING_EMPTY;
	}

	private String getContent(XMLEvent event) {
		return event.asCharacters().getData().trim();
	}

	public List<Flower> getFlowers() {
		if(flowers == null) {
			return Collections.emptyList();
		}

		return flowers;
	}

}