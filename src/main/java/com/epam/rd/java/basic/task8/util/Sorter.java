package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Sorter {

    private static final Comparator<Flower> COMPARE_BY_NAME = new Comparator<>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            int i = o1.getName().compareTo(o2.getName());
            if(i == 0) {
                return COMPARE_BY_ORIGIN.compare(o1, o2);
            }

            return i;
        }
    };

    private static final Comparator<Flower> COMPARE_BY_ORIGIN = new Comparator<>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            int i = o1.getOrigin().compareTo(o2.getOrigin());
            if(i == 0) {
                return COMPARE_BY_LENGTH.compare(o1, o2);
            }

            return i;
        }
    };

    private static final Comparator<Flower> COMPARE_BY_LENGTH = new Comparator<>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            return -o1.getVisualParameters().getAveLenFlower().compareTo(o2.getVisualParameters().getAveLenFlower());
        }
    };

    public static void sortByName(List<Flower> flowers) {
        Collections.sort(flowers, COMPARE_BY_NAME);
    }

    public static void sortByOrigin(List<Flower> flowers) {
        Collections.sort(flowers, COMPARE_BY_ORIGIN);
    }

    public static void sortByLength(List<Flower> flowers) {
        Collections.sort(flowers, COMPARE_BY_LENGTH);
    }

}
