package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.constants.Link;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	private List<Flower> flowers;

	public static void main(String[] args) throws Exception {
		DOMController domController = new DOMController("input.xml");
		domController.parse(true);
		List<Flower> flowers = domController.getFlowers();

		System.out.println(flowers);

		domController.createXML(flowers, "output.dom.xml");
	}

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public void parse(boolean validation) throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		setProperties(validation, dbf);

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(xmlFileName);

		flowers = new DOMHandlerInput().getFlowersFromDOM(document);
	}

	public void createXML(List<Flower> flowers, String xmlFileName) {

		StreamResult streamResult = new StreamResult(new File(xmlFileName));

		TransformerFactory tff = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = tff.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			transformer.transform(new DOMSource(saveToDocument(flowers)), streamResult);
		} catch (TransformerException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public List<Flower> getFlowers() {
		if(flowers == null) {
			return Collections.emptyList();
		}
		return flowers;
	}

	private Document saveToDocument(List<Flower> flowers) throws ParserConfigurationException {
		return new DOMHandlerOutput().addFlowersToDOM(flowers);
	}

	private void setProperties(boolean turnOn, DocumentBuilderFactory dbf) throws ParserConfigurationException {
		dbf.setNamespaceAware(true);

		if(turnOn) {
			dbf.setFeature(Link.FEATURE_TURN_VALIDATION_ON, true);
			dbf.setFeature(Link.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

	}

	private static class DOMHandlerInput {

		public List<Flower> getFlowersFromDOM(Document flowersDOM) {
			NodeList nl = flowersDOM.getElementsByTagName(XML.FLOWER.value());
			if(nl.getLength() == 0) {
				return Collections.emptyList();
			}

			List<Flower> flowers = new ArrayList<>();
			for (int i = 0; i < nl.getLength(); i++) {
				Element flowerDOM = (Element) nl.item(i);
				Flower flower = getFlowerFromDOM(flowerDOM);
				flowers.add(flower);
			}

			return flowers;
		}

		private Flower getFlowerFromDOM(Element flowerDOM) {
			return new Flower(
					getTextFromTag(flowerDOM, XML.NAME.value()),
					Soil.getSoil(getTextFromTag(flowerDOM, XML.SOIL.value())),
					getTextFromTag(flowerDOM, XML.ORIGIN.value()),
					getVisualParametersFromDOM(flowerDOM),
					getGrowingTipsFromDOM(flowerDOM),
					Multiplying.getMultiplying(getTextFromTag(flowerDOM, XML.MULTIPLYING.value()))
			);
		}

		private GrowingTips getGrowingTipsFromDOM(Element growingTipsDOM) {
			return new GrowingTips(
					new BigInteger(getTextFromTag(growingTipsDOM, XML.TEMPERATURE.value())),
					new BigInteger(getTextFromTag(growingTipsDOM, XML.WATERING.value())),
					LightRequiring.getRequiring(getTextFromAttribute(growingTipsDOM,
							XML.LIGHTING.value(), XML.LIGHTING_REQUIRING.value())),
					getTextFromAttribute(growingTipsDOM, XML.TEMPERATURE.value(), XML.TEMPERATURE_MEASURE.value()),
					getTextFromAttribute(growingTipsDOM, XML.WATERING.value(), XML.WATERING_MEASURE.value())
			);
		}

		private VisualParameters getVisualParametersFromDOM(Element visualParametersDOM) {
			return new VisualParameters(
					getTextFromTag(visualParametersDOM, XML.STEAM_COLOUR.value()),
					getTextFromTag(visualParametersDOM, XML.LEAF_COLOUR.value()),
					new BigInteger(getTextFromTag(visualParametersDOM, XML.AVE_LEN_FLOWER.value())),
					getTextFromAttribute(visualParametersDOM, XML.AVE_LEN_FLOWER.value(), XML.LENGTH_MEASURE.value())
			);
		}

		private String getTextFromAttribute(Element tag, String tagName, String attributeName) {
			return ((Element) tag.getElementsByTagName(tagName).item(0)).getAttribute(attributeName);
		}

		private String getTextFromTag(Element tag, String tagName) {
			return tag.getElementsByTagName(tagName).item(0).getTextContent();
		}

	}

	private static class DOMHandlerOutput {

		private Document document;

		public Document addFlowersToDOM(List<Flower> flowers) throws ParserConfigurationException {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			document = db.newDocument();

			Element flowersDOM = document.createElement(XML.FLOWERS.value());
			flowersDOM.setAttribute(Link.ROOT_ATTRIBUTE_NAME_1, Link.ROOT_ATTRIBUTE_VALUE_1);
			flowersDOM.setAttribute(Link.ROOT_ATTRIBUTE_NAME_2, Link.ROOT_ATTRIBUTE_VALUE_2);
			flowersDOM.setAttribute(Link.ROOT_ATTRIBUTE_NAME_3, Link.ROOT_ATTRIBUTE_VALUE_3);
			addFlowerToDOM(flowers, flowersDOM);
			document.appendChild(flowersDOM);

			return document;
		}

		private void addFlowerToDOM(List<Flower> flowers, Element flowersDOM) {

			flowers.forEach(
					flower -> {
						Element flowerDOM = document.createElement(XML.FLOWER.value());

						addTextToDOM(flowerDOM, XML.NAME.value(), flower.getName());
						addTextToDOM(flowerDOM, XML.SOIL.value(), flower.getSoil().value());
						addTextToDOM(flowerDOM, XML.ORIGIN.value(), flower.getOrigin());
						addVisualParametersToDOM(flowerDOM, flower);
						addGrowingTipsToDOM(flowerDOM, flower);
						addTextToDOM(flowerDOM, XML.MULTIPLYING.value(), flower.getMultiplying().value());

						flowersDOM.appendChild(flowerDOM);
					}
			);
		}

		private void addVisualParametersToDOM(Element flowerDOM, Flower flower) {
			Element visualParametersDOM = document.createElement(XML.VISUAL_PARAMETERS.value());

			addTextToDOM(visualParametersDOM, XML.STEAM_COLOUR.value(), flower.getVisualParameters().getSteamColor());
			addTextToDOM(visualParametersDOM, XML.LEAF_COLOUR.value(),  flower.getVisualParameters().getLeafColour());
			addTextToDOM(visualParametersDOM, XML.AVE_LEN_FLOWER.value(),
					flower.getVisualParameters().getAveLenFlower().toString(),
					XML.LENGTH_MEASURE.value(), VisualParameters.getMeasureLength());

			flowerDOM.appendChild(visualParametersDOM);
		}

		private void addGrowingTipsToDOM(Element flowerDOM, Flower flower) {
			Element growingTips = document.createElement(XML.GROWING_TIPS.value());

			addTextToDOM(growingTips, XML.TEMPERATURE.value(),
					flower.getGrowingTips().getTemperature().toString(),
					XML.TEMPERATURE_MEASURE.value(), GrowingTips.getTemperatureMeasure());
			addTextToDOM(growingTips, XML.LIGHTING.value(), XML.LIGHTING_REQUIRING.value(),
					flower.getGrowingTips().getLightRequiring().value());
			addTextToDOM(growingTips, XML.WATERING.value(),
					flower.getGrowingTips().getWatering().toString(),
					XML.WATERING_MEASURE.value(), GrowingTips.getWateringMeasure());

			flowerDOM.appendChild(growingTips);
		}

		private void addTextToDOM(Element parentDOM, String elementName, String nameAttribute, String valueAttribute) {
			Element elementDOM = document.createElement(elementName);
			elementDOM.setAttribute(nameAttribute, valueAttribute);

			parentDOM.appendChild(elementDOM);
		}

		private void addTextToDOM(Element parentDOM, String elementName, String content) {
			Element elementDOM = document.createElement(elementName);
			elementDOM.setTextContent(content);

			parentDOM.appendChild(elementDOM);
		}

		private void addTextToDOM(Element parentDOM, String elementName, String content, String nameAttribute, String valueAttribute) {
			Element elementDOM = document.createElement(elementName);
			elementDOM.setTextContent(content);
			elementDOM.setAttribute(nameAttribute, valueAttribute);

			parentDOM.appendChild(elementDOM);
		}

	}

}
