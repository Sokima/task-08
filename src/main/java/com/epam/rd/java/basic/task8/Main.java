package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.util.Sorter;

import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.parse(true);
		List<Flower> flowersDOM = domController.getFlowers();

		// sort (case 1)
		// PLACE YOUR CODE HERE
		Sorter.sortByName(flowersDOM);

		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.createXML(flowersDOM, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.parse(true);
		List<Flower> flowersSAX = saxController.getFlowers();
		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		Sorter.sortByLength(flowersSAX);
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		domController.createXML(flowersSAX, outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.parse();
		List<Flower> flowersStAX = staxController.getFlowers();
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		Sorter.sortByOrigin(flowersStAX);

		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		domController.createXML(flowersStAX, outputXmlFile);
	}

}
