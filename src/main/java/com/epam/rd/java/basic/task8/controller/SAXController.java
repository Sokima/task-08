package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Link;
import com.epam.rd.java.basic.task8.controller.helpers.Reader;
import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends Reader {
	
	private final String xmlFileName;
	private List<Flower> flowers;

	public static void main(String[] args) throws Exception {

		SAXController saxController = new SAXController("input.xml");
		saxController.parse(true);

		List<Flower> flowers = saxController.getFlowers();

		System.out.println(flowers);

		new DOMController("input.xml").createXML(flowers, "output.sax.xml");
	}

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public void parse(boolean validate) throws ParserConfigurationException, SAXException, IOException {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setNamespaceAware(true);

		if(validate) {
			factory.setFeature(Link.FEATURE_TURN_VALIDATION_ON, true);
			factory.setFeature(Link.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, new SAXHandler());
	}

	public List<Flower> getFlowers() {
		if(flowers == null) {
			return Collections.emptyList();
		}
		return flowers;
	}

	private class SAXHandler extends DefaultHandler {

		@Override
		public void startElement(String uri, String localName, String tagName, Attributes attributes) throws SAXException {
			startProcessing(tagName, getAttribute(attributes));
		}

		@Override
		public void endElement(String uri, String localName, String tagName) throws SAXException {
			endProcessing();
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			contentProcessing(new String(ch, start, length));

		}

		@Override
		public void endDocument() throws SAXException {
			flowers = getCurrentList();
		}

		private String getAttribute(Attributes attributes) {
			return attributes.getLength() > 0 ? attributes.getValue(0) : Link.STRING_EMPTY;
		}
	}

}