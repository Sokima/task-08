package com.epam.rd.java.basic.task8.entity;

public enum Multiplying {
    LEAF("листья"),
    STALK("черенки"),
    SEED("семена");

    private final String typeReproduction;

    Multiplying(String typeSoil) {
        this.typeReproduction = typeSoil;
    }

    public static Multiplying getMultiplying(String type) {
        for(Multiplying el: Multiplying.values()) {
            if(type.equals(el.value())) {
                return el;
            }
        }
        throw new IllegalArgumentException("Provided wrong type of soil");
    }

    public String value() {
        return typeReproduction;
    }

    @Override
    public String toString() {
        return "Multiplying[" +
                "typeMultiplying='" + value() + '\'' +
                ']';
    }
}
