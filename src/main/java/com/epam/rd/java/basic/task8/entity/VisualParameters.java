package com.epam.rd.java.basic.task8.entity;

import java.math.BigInteger;

public class VisualParameters {

    private String steamColor;
    private String leafColour;
    private BigInteger aveLenFlower;

    // attributes
    private static final String measureLength = "cm";

    public VisualParameters() {}

    public VisualParameters(String steamColor, String leafColour, BigInteger aveLenFlower, String providedMeasure) {
        this.steamColor = steamColor;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;

        validation(providedMeasure);
    }

    public String getSteamColor() {
        return steamColor;
    }

    public void setSteamColor(String steamColor) {
        this.steamColor = steamColor;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public BigInteger getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(BigInteger aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public static String getMeasureLength() { return measureLength; }

    @Override
    public String toString() {
        return "VisualParameters[" +
                "steamColor='" + steamColor + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower + " "
                + measureLength +
                ']';
    }

    private void validation(String providedMeasure) {
        if(!measureLength.equals(providedMeasure)) {
            throw new IllegalArgumentException("Provided wrong measure must be cm");
        }
    }

}
