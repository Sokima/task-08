package com.epam.rd.java.basic.task8.entity;

public enum LightRequiring {
    YES("yes"),
    NO("no");

    private final String requiring;

    LightRequiring(String requiring) {
        this.requiring = requiring;
    }

    public static LightRequiring getRequiring(String choice) {
        for(LightRequiring el: LightRequiring.values()) {
            if(choice.equals(el.value())) {
                return el;
            }
        }
        throw new IllegalArgumentException("your argument must be yes or no");
    }

    public String value() {
        return requiring;
    }

    @Override
    public String toString() {
        return "LightRequiring="  + '\'' + requiring + '\'';
    }
}
