package com.epam.rd.java.basic.task8.controller.helpers;

import com.epam.rd.java.basic.task8.constants.Link;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.Attributes;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


public abstract class Reader {

    private String currentElement;
    private String currentAttribute;
    private List<Flower> currentList;

    private static final String FLOWERS = "flowers";
    private static final String FLOWER = "flower";
    private static final String NAME = "name";
    private static final String SOIL = "soil";
    private static final String ORIGIN = "origin";
    private static final String VISUAL_PARAMETERS = "visualParameters";
    private static final String STEM_COLOUR = "stemColour";
    private static final String LEAF_COLOUR = "leafColour";
    private static final String AVE_LEN_FLOWER = "aveLenFlower";
    private static final String GROWING_TIPS = "growingTips";
    private static final String TEMPERATURE = "temperature";
    private static final String LIGHTING = "lighting";
    private static final String WATERING = "watering";
    private static final String MULTIPLYING = "multiplying";


    protected void startProcessing(String tagName, String attribute) {
        currentElement = tagName;
        startProcessing();
        if(!attribute.isEmpty()) {
            currentAttribute = attribute;
        }
    }

    protected void endProcessing() {
        updating();
        currentElement = Link.STRING_EMPTY;
    }

    protected void contentProcessing(String content) {
        updating(content);
    }

    protected List<Flower> getCurrentList() {
        return currentList;
    }

    private void updating(String content) {
        switch (currentElement) {
            case NAME:
                getLastFlower().setName(content);
                break;
            case SOIL:
                getLastFlower().setSoil(content);
                break;
            case ORIGIN:
                getLastFlower().setOrigin(content);
                break;
            case STEM_COLOUR:
                getLastFlower().getVisualParameters().setSteamColor(content);
                break;
            case LEAF_COLOUR:
                getLastFlower().getVisualParameters().setLeafColour(content);
                break;
            case AVE_LEN_FLOWER:
                getLastFlower().getVisualParameters().setAveLenFlower(new BigInteger(content));
                break;
            case GROWING_TIPS:
                getLastFlower().setGrowingTips();
                break;
            case TEMPERATURE:
                getLastFlower().getGrowingTips().setTemperature(new BigInteger(content));
                break;
            case LIGHTING:
                break;
            case WATERING:
                getLastFlower().getGrowingTips().setWatering(new BigInteger(content));
                break;
            case MULTIPLYING:
                getLastFlower().setMultiplying(content);
                break;
        }
    }

    private void startProcessing() {
        switch (currentElement) {
            case FLOWERS:
                currentList = new ArrayList<>();
                break;
            case FLOWER:
                currentList.add(new Flower());
                break;
            case VISUAL_PARAMETERS:
                getLastFlower().setVisualParameters();
                break;
            case GROWING_TIPS:
                getLastFlower().setGrowingTips();
                break;
        }
    }

    private void updating() {
        switch (currentElement) {
            case AVE_LEN_FLOWER:
                if(!currentAttribute.equals(VisualParameters.getMeasureLength()))
                    throw new IllegalArgumentException("Provided wrong attribute");
                break;
            case TEMPERATURE:
                if(!currentAttribute.equals(GrowingTips.getTemperatureMeasure()))
                    throw new IllegalArgumentException("Provided wrong attribute");
                break;
            case LIGHTING:
                getLastFlower().getGrowingTips().setLightRequiring(currentAttribute);
                break;
            case WATERING:
                if (!currentAttribute.equals(GrowingTips.getWateringMeasure()))
                    throw new IllegalArgumentException("Provided wrong attribute");
                break;
        }

        currentAttribute = Link.STRING_EMPTY;
    }

    private Flower getLastFlower() {
        return currentList.get(currentList.size() - 1);
    }

}