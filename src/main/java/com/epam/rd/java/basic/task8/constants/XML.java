package com.epam.rd.java.basic.task8.constants;

public enum XML {
    FLOWERS("flowers"),
    FLOWER("flower"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    VISUAL_PARAMETERS("visualParameters"),
    STEAM_COLOUR("stemColour"),
    LEAF_COLOUR("leafColour"),
    AVE_LEN_FLOWER("aveLenFlower"),
    LENGTH_MEASURE("measure"),
    GROWING_TIPS("growingTips"),
    TEMPERATURE("temperature"),
    TEMPERATURE_MEASURE("measure"),
    LIGHTING("lighting"),
    LIGHTING_REQUIRING("lightRequiring"),
    WATERING("watering"),
    WATERING_MEASURE("measure"),
    MULTIPLYING("multiplying");

    private final String value;

    XML(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
